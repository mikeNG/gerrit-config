import yaml
import requests

from github import Github
from gitlab import Gitlab

from lib import Gerrit, Config

# CalyxOS
GITLAB_GROUP_ID=3642190

if Config.GERRIT_USER and Config.GERRIT_PASS:
    auth = requests.auth.HTTPBasicAuth(Config.GERRIT_USER, Config.GERRIT_PASS)
else:
    auth = None

print("Updating gerrit permissions...")

with open("structure.yml", "r") as f:
    desired_projects = yaml.load(f.read(), Loader=yaml.BaseLoader)

live_projects = Gerrit.get_projects(auth)

changes = {}

for parent, children in desired_projects.items():
    if parent in live_projects:
        if set(live_projects[parent]) == set(desired_projects[parent]):
            continue
        else:
            changes[parent] = list(set(desired_projects[parent]) - set(live_projects[parent]))
            if not changes[parent]:
                del changes[parent]
    else:
        changes[parent] = children

if changes:
    for parent, children in changes.items():
        for child in children:
            Gerrit.update_parent(child, parent, auth)

print("Creating github and gitlab repos...")

gh = Github(Config.GITHUB_TOKEN)
gl = Gitlab(f'https://gitlab.com', Config.GITLAB_TOKEN)
gl_group = gl.groups.get(GITLAB_GROUP_ID)

github_projects = {x.full_name for x in gh.get_organization("CalyxOS").get_repos()}
gitlab_projects = {x.path_with_namespace for x in gl_group.projects.list(all=True, archived=False, visibility="public", include_subgroups=False)}
gitlab_private_projects = {x.path_with_namespace for x in gl_group.projects.list(all=True, archived=False, visibility="private", include_subgroups=False)}
gerrit_projects = set()
private_projects = set()
for parent, children in live_projects.items():
    if parent.startswith("CalyxOS/"):
        gerrit_projects.add(parent)
    for child in children:
        if parent == "Private-Projects":
            private_projects.add(child)
        if child.startswith("CalyxOS/"):
            gerrit_projects.add(child)

github_missing = gerrit_projects - github_projects - private_projects
gitlab_public_missing = gerrit_projects - gitlab_projects - private_projects

for repo in github_missing:
    print(f"Creating {repo} on github...")
    gh.get_organization("CalyxOS").create_repo(repo.replace("CalyxOS/",""), has_wiki=False, has_downloads=False, has_projects=False, has_issues=False, private=False)

for repo in gitlab_public_missing:
    if repo in gitlab_private_projects:
        print(f"Setting {repo} to public on gitlab...")
        gl_project = gl.projects.get(repo)
        gl_project.visibility = "public"
        gl_project.save()
    else:
        print(f"Creating {repo} on gitlab...")
        gl.projects.create({'name': repo.replace("CalyxOS/",""), 'namespace_id': GITLAB_GROUP_ID})

print("Done!")
